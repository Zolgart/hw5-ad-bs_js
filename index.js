

const usersUrl = "https://ajax.test-danit.com/api/json/users";
const postsUrl = "https://ajax.test-danit.com/api/json/posts";



class Card {
    constructor(name, email, title, body, postId) {
        this.name = name;
        this.email = email;
        this.title = title;
        this.body = body;
    }

    createCard() {
        const cardContainer = document.createElement("div");
        cardContainer.classList.add("card");

        cardContainer.insertAdjacentHTML("beforeend", `
             <span>${this.name}</span>
            <span> ${this.email}</span>
            <h1> ${this.title}</h1>
            <p>${this.body}</p>
            <button class="delete-btn">Delete</button>
            `)
        document.body.append(cardContainer)
        return cardContainer;

    }

}

axios.get(usersUrl)

    .then(response => {
        const users = response.data;
        console.log(users)





        axios.get(postsUrl)
            .then(response => {
                const posts = response.data;
                console.log(posts)


                posts.forEach(post => {
                    const user = users.find(user => user.id === post.userId);

                    if (user) {
                        const card = new Card(user.name, user.email, post.title, post.body).createCard()

                        const deleteBtn = card.querySelector(".delete-btn")

                        deleteBtn.addEventListener("click", () => {
                            axios.delete(`https://ajax.test-danit.com/api/json/posts/${post.userId}`)
                                .then((response) => {
                                    console.log(response.status)
                                    if (response.status === 200) {
                                        card.remove();
                                    }
                                })
                                .catch(error => console.warn(error))
                        })
                    }
                })

            })
            .catch(error => console.warn(error))
    })
    .catch(error => console.warn(error))



















